FROM python:3.9

WORKDIR /app

ENV PYTHONUNBUFFERED 1

COPY requirements/*.txt ./
RUN pip install --no-cache-dir -r prod.txt

COPY . .

CMD [ "python", "src/main.py" ]
