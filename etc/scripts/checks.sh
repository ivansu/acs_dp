#!/usr/bin/env sh

# Скрипт для проверки проекта статическими анализаторами кода.
#
# В рамках скрипта осуществляется:
#
# | 1. Проверка правильности расстановки импортов средствами ``isort``.
# | 2. Проверка кода на соответствие ``pep8``.
# | 3. Проверка комментариев к коду на соответствие ``pep257``.
# | 4. Запуск тестов.
#
# Запускать необходимо из корневого каталога проекта.
# Рекомендуется использовать скрипт перед отправкой кода в центральный
# репозиторий.
#
# Вход:
# -st|--skip-tests — если задан, пропускает выполнение тестов.
#
# Выход:
# предупреждения, если таковые имеются.

RUN_TESTS=true

# Проверяет какой код выхода был оставлен последней командой и
# прерывает выполнение, если он неуспешен.
check_exit_code() {
    rc=$?; if [ $rc != 0 ]; then exit $rc; fi
}


# Читаем аргументы.
for i in "$@"
do
case $i in
    -st|--default)
    RUN_TESTS=false
    shift   # past argument with no value
    ;;
    *)
            # unknown option
    ;;
esac
done

# Удаляем кеш.
find . | grep -E "(__pycache__|\.pyc|\.pyo$)" | xargs rm -rf

# Выполняем проверки и сразу выходим, если находим ошибку.
echo "1. Check isort.\n"
isort . -q -c
check_exit_code

echo "2. Check pep8.\n"
pycodestyle .
check_exit_code

echo "3. Check pep257.\n"
pep257 .
check_exit_code

if [ "$RUN_TESTS" = true ] ; then
    echo "4. Running tests.\n"
    coverage run --source src/ tests
    check_exit_code
    echo "\nCoverage report:\n"
    coverage report
fi
check_exit_code

# Завершающие шаги.
exit 0
